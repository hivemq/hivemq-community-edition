FROM openjdk:11-jre-slim

ARG HIVEMQ_CE_VERSION="ce-2019.2-SNAPSHOT"


# Additional JVM options, may be overwritten by user
ENV JAVA_OPTS "-XX:+UseNUMA"

## install unzip
RUN apt-get update && apt-get install unzip

COPY build/zip/hivemq-${HIVEMQ_CE_VERSION}.zip /opt/
# HiveMQ setup
RUN unzip /opt/hivemq-${HIVEMQ_CE_VERSION}.zip  -d /opt/ \
    && rm -f /opt/hivemq-${HIVEMQ_CE_VERSION}.zip \
    && ln -s /opt/hivemq-${HIVEMQ_CE_VERSION} /opt/hivemq \
    # && mv /opt/config.xml /opt/hivemq-ce-${HIVEMQ_CE_VERSION}/conf/config.xml \
    && chmod +x /opt/hivemq/bin/run.sh

# Substitute eval for exec and replace OOM flag if necessary (for older releases). This is necessary for proper signal propagation
RUN sed -i -e 's|eval \\"java\\" "$HOME_OPT" "$JAVA_OPTS" -jar "$JAR_PATH"|exec "java" $HOME_OPT $JAVA_OPTS -jar "$JAR_PATH"|' /opt/hivemq/bin/run.sh && \
    sed -i -e "s|-XX:OnOutOfMemoryError='sleep 5; kill -9 %p'|-XX:+CrashOnOutOfMemoryError|" /opt/hivemq/bin/run.sh

# Make broker data persistent throughout stop/start cycles
VOLUME /opt/hivemq/data

# Persist log data
VOLUME /opt/hivemq/log

EXPOSE 1883

WORKDIR /opt/hivemq

CMD ["/opt/hivemq/bin/run.sh"]
